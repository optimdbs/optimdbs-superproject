function(eigen_project)

set(ep eigen)

## #############################################################################
## List the dependencies of the project
## #############################################################################

list(APPEND ${ep}_dependencies
  ""
  )
  
  
## #############################################################################
## Prepare the project
## ############################################################################# 

EP_Initialisation(${ep}
  USE_SYSTEM OFF 
  BUILD_SHARED_LIBS OFF
  REQUIRED_FOR_PLUGINS OFF
  )


if (NOT USE_SYSTEM_${ep})
## #############################################################################
## Set directories
## #############################################################################

EP_SetDirectories(${ep}
  EP_DIRECTORIES ep_dirs
  )

## #############################################################################
## Define repository for sources
## #############################################################################

set(git_url ${GITHUB_PREFIX}eigenteam/eigen-git-mirror)
message(${git_url})
if(WIN32)
  set(git_tag 9e97af7de76716c99abdbfd4a4acb182ef098808)
else(WIN32)
  set(git_tag master)
endif(WIN32)

## #############################################################################
## Add external-project
## #############################################################################
ExternalProject_Add(${ep}
  ${ep_dirs}
  GIT_REPOSITORY ${git_url}
  GIT_TAG ${git_tag}
  DEPENDS ${${ep}_dependencies}
  CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo "Configure step not required by Eigen."
# no update, build or install command
  UPDATE_COMMAND true
  BUILD_COMMAND true
  INSTALL_COMMAND true
  EXCLUDE_FROM_ALL TRUE
)

## #############################################################################
## Set variable to provide infos about the project
## #############################################################################
ExternalProject_Get_Property(${ep} binary_dir)
set(${ep}_DIR ${binary_dir} PARENT_SCOPE)

## #############################################################################
## Add custom targets
## #############################################################################

EP_AddCustomTargets(${ep})

endif() #NOT USE_SYSTEM_ep

endfunction()
